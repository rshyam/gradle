package com.example.denis.factory;

/**
 * Created by Denis on 06.09.2015.
 */
public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("Inside Circle::draw() method.");
    }
}
