package com.example.denis.objects;

/**
 * Created by Denis on 06.09.2015.
 */
public interface Shape {
    void draw();
}
