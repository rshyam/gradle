package com.example.denis.colors;

/**
 * Created by Denis on 06.09.2015.
 */
public interface Color {
    void fill();
}
