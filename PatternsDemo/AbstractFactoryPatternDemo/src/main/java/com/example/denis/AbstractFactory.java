package com.example.denis;

import com.example.denis.colors.Color;
import com.example.denis.objects.Shape;

/**
 * Created by Denis on 06.09.2015.
 */
public abstract class AbstractFactory {
    abstract Color getColor(String color);
    abstract Shape getShape(String shape) ;
}
