package com.example.denis;

import com.example.denis.colors.Color;
import com.example.denis.objects.Circle;
import com.example.denis.objects.Rectangle;
import com.example.denis.objects.Shape;
import com.example.denis.objects.Square;

/**
 * Created by Denis on 06.09.2015.
 */
public class ShapeFactory extends AbstractFactory {

    @Override
    public Shape getShape(String shapeType){

        if(shapeType == null){
            return null;
        }

        if(shapeType.equalsIgnoreCase("CIRCLE")){
            return new Circle();

        }else if(shapeType.equalsIgnoreCase("RECTANGLE")){
            return new Rectangle();

        }else if(shapeType.equalsIgnoreCase("SQUARE")){
            return new Square();
        }

        return null;
    }

    @Override
    Color getColor(String color) {
        return null;
    }
}
