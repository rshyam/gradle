package com.example.denis;

import com.example.denis.colors.Blue;
import com.example.denis.colors.Color;
import com.example.denis.colors.Green;
import com.example.denis.colors.Red;
import com.example.denis.objects.Shape;

/**
 * Created by Denis on 06.09.2015.
 */
public class ColorFactory extends AbstractFactory {

    @Override
    public Shape getShape(String shapeType){
        return null;
    }

    @Override
    Color getColor(String color) {

        if(color == null){
            return null;
        }

        if(color.equalsIgnoreCase("RED")){
            return new Red();

        }else if(color.equalsIgnoreCase("GREEN")){
            return new Green();

        }else if(color.equalsIgnoreCase("BLUE")){
            return new Blue();
        }

        return null;
    }
}
