package com.example.denis.objects;

/**
 * Created by Denis on 06.09.2015.
 */
public class Square implements Shape {

    @Override
    public void draw() {
        System.out.println("Inside Square::draw() method.");
    }
}
